<?php
namespace MyProject\Model\Classes;

use MyProject\Kernel\AbstractObject;

class Example extends AbstractObject
{
    private $id;
    protected function getId() { return $this->id; }
    protected function setId($value) { $this->id = $value; }
}