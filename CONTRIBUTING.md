# DWWM – Evaluation MVC

Mercredi 12 février 2020
Durée : 2h

## Expression des besoins
La bibliothèque municipale de votre commune souhaite s’équiper d’une application Web-Mobile afin de gérer les emprunts des ouvrages depuis un PC comme une tablette ou un équipement mobil.
Pour cela, elle vous demande de lui proposer une application qui devra permettre de :

* Lister les emprunts en cours (date de début, nom et prénom de l’adhérent)
* Consulter un emprunt : liste des ouvrages empruntés (titre de l’ouvrage)

Lorsqu’un adhérent retourne un ouvrage emprunter on doit pouvoir « cocher » l’ouvrage dans la liste des ouvrages empruntés.

Une fois tous les ouvrages cochés, un bouton « Clôturer » est activé pour permettre de clore un emprunt.

Cet emprunt n’apparait alors plus dans la liste des emprunts en cours.

## Préparation

__Travail à faire__

__Git__

1.	Cloner localement le projet depuis le dépôt distant : 
https://gitlab.com/valarep/dwwm-21-eval-mvc
2.	Aller sur la branche dev
3.	Créer une branche sous la forme dev/Nom-Prenom
4.	Publier votre branche sur le dépôt distant (gitlab)

__MySql__

5.	Créer le compte utilisateur MySQL « bibliotheque » avec le mot de passe « 1234 », 
en cochant la case [X] « Créer une base portant son nom et donner à cet utilisateur tous les privilèges sur cette base. »
6.	Importer la base de données à utiliser : bibliotheque.sql 
(fichier fourni dans le dossier resources/database)
Schéma de la base de données

## UI / UX

__Travail à faire__

7. Consulter les propositions d’interface graphique construite sous Pencil 
(fichiers fournis dans le dossier resources/ui)

8.	Préparer les Templates HTML

    a. emprunt_list.tpl : Affiche la liste des emprunts non clos
    
    Ce Template est à intégrer dans la page d’accueil de l’application

    b.	emprunt_details.tpl : Affiche les détails d’un emprunt avec la liste des ouvrages non rendus de cet emprunt
9.	Commit du travail effectué
10.	Publier votre travail sur le dépôt distant (gitlab)

## Routage

__Travail à faire__

11. Mettre en place les routes suivantes :

| Bouton      | Route                        | Action à réaliser                 |
|-------------|------------------------------|-----------------------------------|
|             | /                            | Afficher la liste des emprunts    |
| [Consulter] | /emprunt/{id}                | Afficher les détails de l’emprunt |
| [Clôturer]  | /emprunt/cloturer/{id}       | Clôturer* l’emprunt               |
| [Valider]   | /emprunt/retour/{id_emprunt} | Valider** le retour des ouvrages  |

12.	Commit du travail effectué
13.	Publier votre travail sur le dépôt distant (gitlab)

## Traitements

__Travail à faire__

14.	Ecrire le code source des traitements associés à chaque route

(*) Clôturer un emprunt
La clôture d’un emprunt s’effectue en enregistrant la date et l’heure de clôture dans la table emprunt

(**) Valider le retour des ouvrages
Cette validation s’effectue en récupérant la liste des ouvrages cochés (chaque ouvrage coché doit renvoyer l’id de son ouvrage). On peut alors mettre à jour dans la table emprunt_ouvrage pour enregistrer la date et l’heure de retour des ouvrages cochés.

15.	Commit du travail effectué
16.	Publier votre travail sur le dépôt distant (gitlab)

Bravo, vous avez fini votre travail 😊
